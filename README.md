# Application Chenilles Animées

Ce projet contient le code source (sous la forme d'un projet maven d'application java) d'une solution
 du [TP JAVA Chenilles Animées](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp09_chenilles/tp09_Chenilles.html)
 du cours Java du Master 2 Compétences Complémentaires en Informatique (M2CCI).

Pour récupérer les sources de ce projet vous pouvez

* soit utiliser git en effectuant l'une des commande de clonage du projet

  * via HTTPS
   ```
   git clone https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/chenilles-animees.git
   ```

  * via SSH
   ```
   git clone git@gricad-gitlab.univ-grenoble-alpes.fr:enseignement1/m2cci/java/chenilles-animees.git
   ```
* soit récupérer une archive (.tar.gz ou .zip) de l'une des [releases](https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/chenilles-animees/-/releases/) du projet

## Différentes Applications

Ce projet contient différentes Applications (classes avec méthode *main*) :

- **AppliChenilles** qui permet d'afficher une ou plusieurs chenilles (le nombre de chenilles pouvant être définit comme paramètre de la ligne de commandes)

- **AppliChenillesCouleur** qui permet d'afficher une ou plusieurs chenilles coloriées, leur nombre pouvant être défini comme paramètre de la ligne de commandes, leur couleur étant tirée au hazard

- **AppliChenillesStarWars** qui permet d'afficher 4 chenilles dont la tête
est soit une image d'un personnage de lasaga Star Wars (pricncesse Leila, Darth Vador, C3PO ou stormtrooper)

- **AppliNChenilles** qui permet d'afficher des chenilles de tous types, le nombre et le type des chenilles ainsi que le délai entre deux pas d'animation pouvant être fixés via la ligne de commandes. Les différentes options sont :

```
Option              Description
------              -----------
-?, -h              show help
--coul <Integer>    count of colored caterpillars (default: 3)
--delay <Integer>   delay (in ms) between two animation frames (default: 80)
--normal <Integer>  count of normal (Black) caterpillars (default: 2)
--sw <Integer>      count of SW caterpillars (default: 4)
```

Pour faciliter la gestion de ces arguments la librairie open source [JOpt-simple](https://github.com/jopt-simple/jopt-simple) est utilisée. Pour plus d'informations voir sa [documentation](http://jopt-simple.github.io/jopt-simple).
       

Ce projet utilise [Maven Wrapper](https://maven.apache.org/wrapper/) pour être certain que la bonne version de maven sera utilisée, invoquez  `mvnw` plutôt que `mvn`.  (Pour en savoir plus sur Maven Wrapper : https://javastreets.com/blog/maven-wrapper.html)
