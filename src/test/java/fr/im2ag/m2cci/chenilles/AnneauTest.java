
package fr.im2ag.m2cci.chenilles;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnneauTest {

    // constantes définissant les caractéristiques de deux anneaux,
    // a1 et a2 qui seront réinitialisé avant chaque cas de test
    private static final int A1_X = 10;
    private static final int A1_Y = 18;
    private static final int A1_R = A1_Y;

    private static final int A2_X = 15;
    private static final int A2_Y = 17;


    private Anneau a1;
    private Anneau a2;

    /**
     * réinitialise a1 et a2 avant chaque cas de test
     */
    @BeforeEach
    void setUp() {
        a1 = new Anneau(A1_X, A1_Y, A1_R);
        a2 = new Anneau(A2_X, A2_Y);
    }


    @Test
    void testGetR() {
        assertEquals(A1_R, a1.getR());
        assertEquals(Anneau.DEFAULT_R, a2.getR());
    }

    @Test
    void testGetX() {
        assertEquals(A1_X, a1.getX());
        assertEquals(A2_X, a2.getX());
    }

    @Test
    void testGetY() {
        assertEquals(A1_Y, a1.getY());
        assertEquals(A2_Y, a2.getY());
    }

    @Test
    void testPlacerA() {
        a1.placerA(a2);
        // vérification du nouvel état de a1
        assertEquals(A1_R, a1.getR()); // le rayon de a1 n'a pas changé
        assertEquals(A2_X, a1.getX()); // son x est celui de a2
        assertEquals(A2_Y, a1.getY()); // son y est celui de a2
        // vérification que l'état de a2 n'a pas changé
        assertEquals(A2_X, a2.getX());
        assertEquals(A2_Y, a2.getY());
        assertEquals(Anneau.DEFAULT_R, a2.getR());
    }

    @Test
    void testPlacerA2() {
        a1.placerA(40, 55);
        // vérification du nouvel état de a1
        assertEquals(A1_R, a1.getR()); // le rayon de a1 n'a pas changé
        assertEquals(40, a1.getX());
        assertEquals(55, a1.getY());
    }
}


