/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.chenilles;

/**
 * Dessin qui  contient des chenilles Animées
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
import java.awt.Graphics;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;

/**
 * Defini le contenu de la fenêtre de l'application d'animation des Chenilles.
 * Une zone de dessin est un JPanel qui gère un liste d'objets Chenille.
 * Lorsqu'il se réaffiche l'objet Dessin redessinne les différents objets
 * Chenille contenus dans cette liste.
 *
 * @author Philippe Genoud
 */
public class Dessin extends JPanel {

    /**
     * stocke la liste des Chenille ayant été ajoutées à cette zone de dessin.
     */
    private final List<Chenille> listeDesChenilles = new CopyOnWriteArrayList<>();

    /**
     * retourne la largeur de la zone de dessin.
     *
     * @return la largeur.
     */
    public int getLargeur() {
        return getWidth();
    }

    /**
     * retourne la hauteur de la zone de dessin.
     *
     * @return la hauteur.
     */
    public int getHauteur() {
        return getHeight();
    }

    /**
     * ajoute un Chenille à la zone de dessin.
     *
     * @param c la Chenille à ajouter au Dessin
     * @see Chenille
     */
    public void ajouterObjet(Chenille c) {

        if (!listeDesChenilles.contains(c)) {
            // l'objet n'est pas déjà dans la liste
            // on le rajoute à la liste des objets du dessin
            listeDesChenilles.add(c);
            // le dessin se réaffiche
            repaint();
            this.pause(10);
        }
    }

    /**
     * temporisation de l'animation.
     * @param duree delai de temporisation en ms.
     */
    public void pause(int duree) {
        try {
            Thread.sleep(duree);
        } catch (Exception e) {
        }
    }

    /**
     * affiche la zone de dessin et son contenu
     *
     * @param g le contexte graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // on dessine chacun des visages contenus dans la zone de dessin
        for (Chenille c : listeDesChenilles) {
            c.dessiner(g);
        }
    }

    /**
     * fait faire un déplacement élémentaire à toutes les chenilles
     *  situées dans la zone de dessin
     */
    public void deplacerChenilles() {
        for (Chenille c : listeDesChenilles) {
            c.deplacer();
        }
    }

} // Dessin

